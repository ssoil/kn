package kn

import (
	"bufio"
	"encoding/json"
	"io"
	"os"
)

// Heading is a heading encountered in a Node Markdown file.
type Heading struct {
	Level int
	Ident string
	Title string
}

// String fulfills the Stringer interface as a simple JSON array
func (h Heading) String() string { return ConvertToJSON(h) }

// ReadHeadings parses all Pandoc Markdown ATX-style headings (initial
// hashtags) from the provided source. Returns error if not ATX or
// not 1-6 hashtags followed by a single space. Each heading must
// be on a single line.
func ReadHeadings(r io.Reader) []Heading {
	hs := []Heading{}

	// states
	inCode := false
	fence := ""
	fencel := 0
	wasBlank := true
	inYAML := false

	last := ""

	s := bufio.NewScanner(r)
	for s.Scan() {
		line := s.Text()
		n := len(line)

		if n == 0 || (n > 0 && line[0] == ' ') {
			wasBlank = true
			last = ""
			continue
		}

		switch {
		case len(last) > 0 && len(line) >= 1 && line[0] == '-':
			// FIXME to stop reading tables as headings
			hs = append(hs, Heading{2, AutoIdent(last), last})
			continue
		case !wasBlank:
			continue
		case inYAML:
			if n == 3 && line == "---" {
				inYAML = false
			}
			continue
		case inCode:
			if n >= fencel && line[0:fencel] == fence {
				inCode = false
				fence = ""
				fencel = 0
			}
			continue
		case n >= 3 && line[0:3] == "## ":
			md := line[3:]
			hs = append(hs, Heading{2, AutoIdent(md), md})
		case n >= 4 && line[0:4] == "### ":
			md := line[4:]
			hs = append(hs, Heading{3, AutoIdent(md), md})
		case n >= 5 && line[0:5] == "#### ":
			md := line[5:]
			hs = append(hs, Heading{4, AutoIdent(md), md})
		case n >= 6 && line[0:6] == "##### ":
			md := line[6:]
			hs = append(hs, Heading{5, AutoIdent(md), md})
		case n >= 7 && line[0:7] == "###### ":
			md := line[7:]
			hs = append(hs, Heading{6, AutoIdent(md), md})
		case n >= 2 && line[0:2] == "# ":
			md := line[2:]
			hs = append(hs, Heading{1, AutoIdent(md), md})
		case n == 3 && line == "---":
			inYAML = true
			continue
		case n >= 4 && line[0:4] == "~~~~":
			inCode = true
			fence = "~~~~"
			fencel = 4
			continue
		case n >= 3 && line[0:3] == "~~~":
			inCode = true
			fence = "~~~"
			fencel = 3
			continue
		case n >= 3 && line[0:3] == "```":
			inCode = true
			fence = "```"
			fencel = 3
			continue
		}
		last = line
		wasBlank = false
	}

	return hs
}

// LoadHeadings loads the headings from the specified file.
func LoadHeadings(path string) []Heading {
	file, _ := os.Open(path)
	defer file.Close()
	return ReadHeadings(file)
}

// SubNode is a content subdivision of a Node usually represented by headings
// but with Children in a tree data structure.
type SubNode struct {
	Level    int
	Ident    string
	Title    string
	Children []*SubNode
}

// String fulfills the Stringer interface as long JSON.
func (n SubNode) String() string { return ConvertToJSON(n) }

// MarshalJSON fulfill the interface.
func (n SubNode) MarshalJSON() ([]byte, error) {
	t := []interface{}{n.Level, n.Ident, n.Title, n.Children}
	return json.Marshal(t)
}

func (n *SubNode) Add(s *SubNode) {
	n.Children = append(n.Children, s)
}

// ReadSubNodes identifies and returns SubNodes ignoring any level one
// headings.
func ReadSubNodes(r io.Reader) []*SubNode {
	root := new(SubNode)
	root.Children = []*SubNode{}
	parents := [7]*SubNode{}
	parents[1] = root
	last := root
	hs := ReadHeadings(r)
	for _, h := range hs {
		if h.Level == 1 {
			continue
		}
		n := &SubNode{h.Level, h.Ident, h.Title, []*SubNode{}}
		if n.Level > last.Level {
			last.Add(n)
		} else {
			i := n.Level - 1
			for ; parents[i] == nil; i-- {
			}
			parents[i].Add(n)
		}
		last = n
		parents[n.Level] = n
	}
	return root.Children
}
