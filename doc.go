/*

Unless otherwise noted most procedures are not safe for concurrency. This is to
maxmize performance for the most common use cases. For design purposes, modules
should be considered components that cannot be manipulated asyncronously while
nodes definitely can, provided their parent module is re-indexed afterward.

*/
package kn
