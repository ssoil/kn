package kn_test

import (
	"fmt"

	"gitlab.com/knetone/kn"
)

func ExampleNewIdent() {

	// These are the examples from the Pandoc Markdown documentation:

	fmt.Println(kn.AutoIdent("Heading identifiers in HTML"))
	fmt.Println(kn.AutoIdent("Maître d'hôtel"))
	fmt.Println(kn.AutoIdent("Dogs*?--in *my* house?"))
	fmt.Println(kn.AutoIdent("Dogs*?--in *my* house? {#dogs}"))
	fmt.Println(kn.AutoIdent("Dogs*?--in *my* house? {.some #dogs}"))
	fmt.Println(kn.AutoIdent("Dogs*?--in *my* house? {.some #dogs #and}"))
	fmt.Println(kn.AutoIdent("Dogs*?--in *my* house? {#dogs .some}"))
	fmt.Println(kn.AutoIdent("[HTML], [S5], or [RTF]?"))
	fmt.Println(kn.AutoIdent("3. Applications"))
	fmt.Println(kn.AutoIdent("33"))
	fmt.Println(kn.AutoIdent("KN^3.v3"))

	// Output:
	//
	// heading-identifiers-in-html
	// maître-dhôtel
	// dogs--in-my-house
	// dogs
	// dogs
	// and
	// dogs
	// html-s5-or-rtf
	// applications
	//
	// kn3.v3
}
