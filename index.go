package kn

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	fp "path/filepath"
	"time"
)

// VERSION indicates the KnowledgeNet specification version represented in this
// package.
const VERSION = 0.8

// DATE is the format for all KnowledgeNet dates.
const DATE = "2006-01-02"

// TSTAMP is the format for all KnowledgeNet time stamps and compatible with
// ISO 8601, PostgreSQL, and other databases.
const TSTAMP = "2006-01-02 15:04:05"

// Module canonizes the specific meta data for a stanard KnowledgeNet
// module containing one or more nodes. This data is always available in the
// kmod.json file and is derived from the main README.md YAML front matter
// in the root module directory. Although this summary is similar to Node
// by design to provide defaults for every node when not explicitely defined
// for each node (to preserve space) it is distinct and available from the root
// of the kmod.json tree structure. Names match Pandoc were convention exists.
// Ident should be the name of the domain if module is also a hosted site.
type Module struct {
	KN         float32        `json:"kn,omitempty"`         // knowledgenet specification version
	Ident      string         `json:"ident,omitempty`       // no spaces (pandoc autoid), matches dir or domain
	Subtitle   string         `json:"subtitle,omitempty"`   // subtitle (simplified markdown)
	Abstract   string         `json:"abstract,omitempty"`   // text summary of module (simplified markdown)
	Author     string         `json:"author,omitempty"`     // primary module author
	Authors    map[string]int `json:"authors,omitempty"`    // authors and number of nodes authored
	Location   string         `json:"location"`             // human friendly location ID for date/time (IANA)
	Source     string         `json:"source,omitempty"`     // full URI to location of source code
	Issues     string         `json:"issues,omitempty"`     // full URI for submitting issues about source
	Date       string         `json:"date,omitempty"`       // original creation date
	Updated    string         `json:"updated,omitempty"`    // last date any content significantly updated
	Modified   int64          `json:"modified,omitempty"`   // last detected modification to any content source
	Formats    []string       `json:"formats,omitempty"`    // all formats used in module
	Categories []string       `json:"categories,omitempty"` // all categories contained in module
	Keywords   []string       `json:"keywords,omitempty"`   // list of keywords for module, do not abuse
}

// String fulfills the Stringer interface as JSON.
func (m Module) String() string { return ConvertToJSON(m) }

// LoadModule loads the module at modpath and returns a new Module object. Note
// that LoadYAML(mreadme) may be preferable so the overriding data can be combined
// with a structure that is less strict.
func LoadModule(modpath string) Module {
	m := Module{}
	byt, err := ioutil.ReadFile(fp.Join(modpath, "knmod.json"))
	if err != nil {
		return m
	}
	json.Unmarshal(byt, &m)
	return m
}

// Node canonizes the specific meta data for a standard
// KnowledgeNet node. This is gauranteed never to have any breaking
// change to foster maximum compatibility between tools reading index summaries
// of KnowledgeNet modules. The serialized/marshalled version is always
// the capitalized first letter of each field. In most cases these words are
// the same as those standardized by Pandoc templates.
type Node struct {
	Format   string     // standardized format identifier (ex: article)
	Ident    string     // no spaces (pandoc autoid), matches dir
	Title    string     // title (simplified markdown), becomes h1
	Subtitle string     // subtitle (simplified markdown)
	Authors  []string   // list of authors with with optional email
	Date     string     // original creation date
	Updated  string     // last date content significantly updated
	Modified int64      // last detected modification to any content source
	Category string     // creator-defined categories
	Keywords []string   // list of keywords, do not abuse
	Abstract string     // text summary of node (simplified markdown)
	Headings []*SubNode // headings (Level,Ident,Title,Subs)
}

// String fulfills the Stringer interface as JSON.
func (ns Node) String() string { return ConvertToJSON(ns) }

// MarshalJSON fulfills the interface.
func (ns Node) MarshalJSON() ([]byte, error) {
	t := []interface{}{
		ns.Format, ns.Ident,
		ns.Title, ns.Subtitle,
		ns.Authors,
		ns.Date, ns.Updated, ns.Modified,
		ns.Category, ns.Keywords,
		ns.Abstract,
		ns.Headings,
	}
	return json.Marshal(t)
}

// UnmarshalJSON fulfills the interface.
func (ns *Node) UnmarshalJSON(byt []byte) error {
	t := []interface{}{}
	if err := json.Unmarshal(byt, &t); err != nil {
		return err
	}
	ns.Format = t[0].(string)
	ns.Title = t[1].(string)
	ns.Subtitle = t[2].(string)
	ns.Authors = t[3].([]string)
	ns.Date = t[4].(string)
	ns.Updated = t[5].(string)
	ns.Modified = t[6].(int64)
	ns.Category = t[7].(string)
	ns.Keywords = t[8].([]string)
	ns.Abstract = t[9].(string)
	ns.Headings = t[10].([]*SubNode)
	return nil
}

// MakeIndex produces the knmod.json and kndex.json files for the module at
// modpath. Always writes even if there are errors in other stages of node
// parsing and summarization.
func MakeIndex(modpath string) {
	data := LoadYAML(fp.Join(modpath, "README.md"))
	nodes := DATA{}

	categories := map[string]int{}
	formats := map[string]int{}
	authors := map[string]int{}

	var date, updated time.Time
	var modified int64

	npaths := NodePaths(modpath)
	for _, npath := range npaths {
		file := fp.Join(npath, "README.md")
		readme, _ := os.Open(file)
		d := ReadYAML(readme)
		readme.Seek(0, 0)
		ns := Node{}
		ns.Headings = ReadSubNodes(readme)
		readme.Close()
		ns.Ident = path.Base(npath)
		if v, has := d["title"]; has {
			ns.Title = v.(string)
		}
		if v, has := d["subtitle"]; has {
			ns.Subtitle = v.(string)
		}
		if v, has := d["abstract"]; has {
			ns.Abstract = v.(string)
		}
		if v, has := d["date"]; has {
			ns.Date = v.(string)
			nd, err := time.Parse(DATE, ns.Date)
			if err == nil && (date.IsZero() || nd.Before(date)) {
				date = nd
			}
		}
		if v, has := d["updated"]; has {
			ns.Updated = v.(string)
			nu, err := time.Parse(TSTAMP, ns.Updated)
			if err == nil && date.Before(updated) {
				updated = nu
			}
		}
		if v, has := d["category"]; has {
			ns.Category = v.(string)
			categories[v.(string)]++
		}
		if v, has := d["format"]; has {
			ns.Format = v.(string)
			formats[ns.Format]++
		}
		if kw, has := d["keywords"]; has {
			ns.Keywords = []string{}
			for _, v := range kw.([]interface{}) {
				ns.Keywords = append(ns.Keywords, v.(string))
			}
		}
		if av, has := d["author"]; has {
			ns.Authors = []string{}
			switch a := av.(type) {
			case string:
				ns.Authors = append(ns.Authors, a)
				authors[a]++
			case []interface{}:
				for _, v := range av.([]interface{}) {
					ns.Authors = append(ns.Authors, v.(string))
					authors[v.(string)]++
				}
			}
		}
		info, _ := os.Stat(file)
		mt := info.ModTime()
		ns.Modified = mt.Unix()
		if modified < ns.Modified {
			modified = ns.Modified
		}
		nodes[ns.Ident] = ns
	}

	if !date.IsZero() {
		data["date"] = date.Format(DATE)
	}
	if !updated.IsZero() {
		data["updated"] = updated.Format(TSTAMP)
	}
	if _, has := data["timezone"]; !has {
		loc := DetectLocationTZ()
		if len(loc) > 0 {
			data["timezone"] = loc
		}
	}

	data["modified"] = modified
	data["categories"] = categories
	data["formats"] = formats
	data["authors"] = authors
	data["kn"] = VERSION

	data.Save(fp.Join(modpath, "knmod.json"))
	nodes.SaveTiny(fp.Join(modpath, "kndex.json"))

}

/*
// LoadIndex loads the index for module at modpath and returns it.
func LoadIndex(modpath string) DATA {
	data := LoadYAML(fp.Join(modpath, "README.md"))
	nodes := DATA{}
	//TODO finish
	return nodes
}
*/
