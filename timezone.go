package kn

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// DetectLocationTZ uses the ipinfo.io service (if available) to safely
// detect the timezone location based ISP. Do not depend on this when
// using a VPN (unless all your date and time information in your content
// also agrees with the location of your VPN). The equivalent of this
// function can be done from the shell with the `curl ipinfo.io` command.
func DetectLocationTZ() string {
	resp, _ := http.Get("http://ipinfo.io")
	defer resp.Body.Close()
	buf, _ := ioutil.ReadAll(resp.Body)
	d := map[string]interface{}{}
	json.Unmarshal(buf, &d)
	if v, has := d["timezone"]; has {
		return v.(string)
	}
	return ""
}
