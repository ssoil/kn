package kn_test

import (
	"fmt"

	"gitlab.com/skilstak/soil/kn/go"
)

func ExampleNewIdent() {

	fmt.Println(kn.ToIdent("Heading identifiers in HTML"))
	fmt.Println(kn.ToIdent("Maître d'hôtel"))
	fmt.Println(kn.ToIdent("Dogs*?--in *my* house?"))
	fmt.Println(kn.ToIdent("[HTML], [S5], or [RTF]?"))
	fmt.Println(kn.ToIdent("3. Applications"))
	fmt.Println(kn.ToIdent("33"))
	fmt.Println(kn.ToIdent("KN^3.v3"))

	// Output:
	//
	// heading-identifiers-in-html
	// maître-dhôtel
	// dogs--in-my-house
	// html-s5-or-rtf
	// applications
	//
	// kn3.v3
}
