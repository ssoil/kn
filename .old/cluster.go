package kn

import (
	"io/ioutil"
	"path/filepath"
)

// Cluster represents a cluster of Nodes. The terms Cluster and Node are used
// instead of page, document, and site because each Node does not require the
// Web for distribution or viewing. (Besides, a spider's web was always
// a horrible metaphor.)
//
// Clusters very closely resemble clusters of neurons. Each Node is attached to
// several other Nodes but the Node itself is independent, flat. It doesn't
// know it is a part of a cluster, only about its immediate attachments.
//
// Nodes are therefore organized into a flat directory structure. (See
// Node.Ref).
type Cluster struct {
	Authors []string
	Issues  string
	Source  string
	// TODO Headings with Abstracts?
}

// LocalCluster is one that is opened on localhost using only the local
// filesystem for access.
//
// Root is the path to the local directory containing the Cluster with
// all of its nodes.
type LocalCluster struct {
	Cluster
	Root string
}

// OpenLocalCluster opens the LocalCluster. Returns nil and error if nothing
// there or opening fails.
func OpenLocalCluster(root string) (*LocalCluster, error) {
	if !FileExists(root) {
		return nil, Error(NoLocalRootFound, SolRed, SolCyan, root)
	}
	lc := new(LocalCluster)
	lc.Root = root
	return lc, nil
}

// ListNodes scans the Root for all nodes ignoring directories that begin with
// underscore (_) or period (.) and returns the list of Node identifiers. It
// does not load or index any of the node information always returning what is
// currently in the Cluster root. Indeed, ListNodes is used by other methods
// to do such indexing and loading.
func (c LocalCluster) ListNodes() []string {
	nodes := []string{}
	if c.Root == "" {
		return nodes
	}
	files, err := ioutil.ReadDir(c.Root)
	if err != nil {
		return nodes
	}
	for _, f := range files {
		name := f.Name()
		if !f.IsDir() || name[0] == '.' || name[0] == '_' {
			continue
		}
		nodes = append(nodes, name)
	}
	return nodes
}

// NodesNeedBuild checks if the given list of Nodes (by Ident) are in need of
// building (meta.yml or README.md is newer than index.json or index.html). If
// no arguments are passed it checks the entire cluster of Nodes sequentially.
//
// (This may be reimplemented with multiple concurrent goroutines all checking
// in a future version to increase response time if needed.)
func (c LocalCluster) NodesNeedBuild(nodes ...string) []string {
	if len(nodes) == 0 {
		nodes = c.ListNodes()
	}
	need := []string{}
	for _, node := range nodes {
		readme := filepath.Join(c.Root, node, "README.md")
		meta := filepath.Join(c.Root, node, "meta.yml")
		ihtml := filepath.Join(c.Root, node, "index.html")
		ijson := filepath.Join(c.Root, node, "index.json")
		if PathIsNewer(readme, ihtml, ijson) || PathIsNewer(meta, ihtml, ijson) {
			need = append(need, node)
		}
	}
	return need
}

// NeedsFullBuild examines these cluster-wide files for changes:
//
//     * _/meta.yml       -> index.json
//     * _/template.html  -> index.html
//
// Changes to the following standard template files should never
// trigger a new build (although template.html always should):
//
//     * _/main.js
//     * _/main.css
//
// Other files within the special _ template directory should also
// never trigger a build.
func (c LocalCluster) NeedsFullBuild() bool {
	meta := filepath.Join(c.Root, "_", "meta.yml")
	template := filepath.Join(c.Root, "_", "template.html")
	ijson := filepath.Join(c.Root, "index.json")
	ihtml := filepath.Join(c.Root, "index.html")
	return PathIsNewer(meta, ijson) || PathIsNewer(template, ihtml)
}
