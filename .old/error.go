package kn

import (
	"fmt"
	"os"
)

// Exit prints the error (anything with Stringer interface) and exits with 1 exit value.
func Exit(err interface{}) {
	fmt.Printf("%v%v%v\n", SolRed, err, Reset)
	os.Exit(1)
}

// Error is the package-level error handler. Calling Error never panics or
// exists. Call Exit for that if needed.
func Error(msg string, args ...interface{}) error {
	return fmt.Errorf(msg, args...)
}
