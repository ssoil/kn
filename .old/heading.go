package kn

import (
	"bufio"
	"os"
)

// ParseHeadings returns all the headings from Markdown document so long as
// only ``` is used to create sections to be ignored and the document folows
// the EzMark conventions of only using hashtags (#) to mark headings and not
// using any markup at all in the heading itself (which is completely
// unnecessary when creating Knowledge Nodes). This allows for very efficient
// indexing of all content.
func ParseHeadings(path string) []Heading {
	headings := []Heading{}
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		return headings
	}
	scanner := bufio.NewScanner(file)
	ignore := false
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			continue
		}
		if line[0:3] == "```" {
			if ignore {
				ignore = false
			} else {
				ignore = true
			}
			continue
		}
		if ignore {
			continue
		}
		if line[0] == '#' {
			var level int
			switch {
			case line[0:2] == "# ":
				level = 1
				line = line[2:]
			case line[0:3] == "## ":
				level = 2
				line = line[3:]
			case line[0:4] == "### ":
				level = 3
				line = line[4:]
			case line[0:5] == "#### ":
				level = 4
				line = line[5:]
			case line[0:6] == "##### ":
				level = 5
				line = line[6:]
			case line[0:7] == "###### ":
				level = 6
				line = line[7:]
			}
			headings = append(headings, Heading{line, level, ToIdent(line)})
		}
	}
	return headings
}
