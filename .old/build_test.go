package kn

import (
	"os"
	"testing"
)

// look for testing/blah/index.html to validate
func TestBuild(t *testing.T) {
	os.Setenv("KNROOT", "testing")
	Build("testing/blah")
}
