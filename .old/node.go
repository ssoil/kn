package kn

import (
//	"encoding/json"
//	"io/ioutil"
//"path/filepath"
)

// Heading represents a basic content heading and is returned from Headings.
//
// Ident can be populated quickly by calling SetIdent() on the object which
// calls ToIdent() passing it the Headings own Text field. This allows Headings
// to exist where the one-to-one relationship between Text and Ident may not
// necessarily exist although it is strongly encouraged. Ident is public for
// this reason as well since it can be Marshalled.
type Heading struct {
	Text  string
	Level int
	Ident string
}

func (h *Heading) SetIdent() {
	h.Ident = ToIdent(h.Text)
}

// Converts to JSON when used as a string.
func (o Heading) String() string { return JSON(o) }

// A Node represents a Knowedge Node that belongs to a Cluster but has no
// knowledge about the cluster to which it belongs, only its uniq Ident within
// the Cluster.
//
// All Nodes are contained in a flat, 1-level file directory structure
// within the Cluster. Nodes can be freely moved from one Cluster to another
// so long as the Node.Ref remains unique within any given Cluster containing
// it.
//
// Ident follows the requirements for all Ident types but does not need to match
// an Ident that would be created from Title if it were passed to ToIdent().
// This allows separation between the Ident and the Title so that shorter
// Idents can be used for rather long titles (ex: vim -> "Vi / Vim Editor").
// This becomes particularly useful when attaching (linking to) Nodes while
// creating others since only the short Ident need be typed. The $ident$ is
// available to the template and is written to the index.json file.
//
// The Title can be any unformatted text and is derived from the first level
// one (h1, #) heading. Setting Title in the meta.yml file is unnecessary
// and will be ignored. Nodes with README.md files without one and only one
// first level heading as the first of the document are considered invalid
// and will not Build. Title is available at $title$ in pandoc templates and
// is written to index.json.
//
// Authors are noted in the meta.yml file. Authorship of a Cluster
// is derived from the Authors of its Nodes. When no Authors are found
// the default Cluster.Authors will be used instead. Authors can be included
// in the pandoc template with the $authors$ template variable. Authors
// are written to the index.json file.
//
// The Abstract (a term Pandoc uses for summary) will be taken from the
// meta.yml file or the first paragraph of README.md up to the first period.
// The Abstract is available as the $abstract$ template tag an is written
// to the index.json file.
//
// Keywords are taken from the meta.yml file only and can be included in the
// index.html by adding them to the template used during the Cluster.Build().
// Keywords are available as $keywords$ to templates and are written to
// index.json.
//
// Headings are extracted from README.md and must use the hashtag (ATX) form
// and contain no inline formatting. (See Heading) Headings are available as
// indirectly via the pandoc $toc$ tag and are written to index.json.
//
// Attached are the Idents of the Nodes to which this Node links. They are
// identified by parsing any Markdown link with the format [some text](/node/).
// Note the beginning and trailing slashes are required. These are note
// available in any template tag form, but are written to the index.json file.
//
// External Links are also captured in the index for reference and to check
// for link integrity of dependencies over time. They are written to he
// index.json file.
//
// LastMod is the last modified date of the index.html file and is available
// within the pandoc template as $lastmod$ (ISO) and $epoch$ (which can be used
// by any language and allows real-time display of age when combined with
// JavaScript).
//
// Local links to content within this Node (relative URLs) are considered to
// compose the content of the Node and are therefore not indexed.
//
// Nodes must not embed any external content thus preserving the offline-first
// focus enabled by progressive web apps and other local saving and
// distribution. Indeed, there is no requirement for a network at all.
//
// Like neurons, Nodes cannot be composed of other Nodes instead, Nodes should
// link to one another. There is, however, nothing preventing an author
// from creating a Node that contains nothing but links to other Nodes.
type Node struct {
	Ident    string
	Title    string
	Authors  []string
	Abstract string
	Keywords []string
	Headings []Heading
	Attached []string
	External []string
	LastMod  int64
}

// Converts to JSON when used as a string.
func (o Node) String() string { return JSON(o) }

/*
func OpenNode(path ...string) Node {
	n := Node{}
	if len(args) > 0 {
		path := args[0]
		meta := filepath.Join(path, "meta.yml")
		js := filepath.Join(path, "index.json")
		html := filepath.Join(path, "index.html")
		if PathIsFresher(readme, html) {
			return Build(readme)
		}
		byt, err := ioutil.ReadFile(json)
		if err != nil {
			Exit(err)
		}
		json.Unmarshal(byt, &n)
	}
	return n
}
*/
