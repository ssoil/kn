package kn

import (
	"strings"
	"unicode"
)

// SpecialRunes contains the special runes
var SpecialRunes = []rune(" \t\r\n\f~`!@#$%^&*()-_+=[]{}|\\;:\"'<>,.?/")

// CollapseSpaces collapses all spaces into a single space.
func CollapseSpaces(str string) string {
	lastWas := false
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			if lastWas {
				return -1
			}
			lastWas = true
		} else {
			lastWas = false
		}
		return r
	}, str)
}

// CollapseRunes collapses all of the specified runes into a single rune.
func CollapseRunes(str string, cr rune) string {
	lastWas := false
	return strings.Map(func(r rune) rune {
		if r == cr {
			if lastWas {
				return -1
			}
			lastWas = true
		} else {
			lastWas = false
		}
		return r
	}, str)
}

// RemoveSpaces returns a string free of any spaces as defined by Unicode.
func RemoveSpaces(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, str)
}

// ReplaceRune replaces a specific rune from a specific set with another rune as
// efficiently as possible.
func ReplaceRune(str string, from []rune, to rune) string {
	return strings.Map(func(r rune) rune {
		for _, f := range from {
			if r == f {
				return to
			}
		}
		return r
	}, str)
}

// Slugify normalizes unicode text into URL-friendly slugs according to the same rules used in Vue
func Slugify(str string) string {
	return strings.ToLower(strings.Trim(CollapseRunes(ReplaceRune(str, SpecialRunes, '-'), '-'), "-"))
}

// FirstLine returns the first line of the string without the line ending.
// Specifically if the '\r' or '\n' runes are detected.
func FirstLine(str string) string {
	var line []rune
	for _, r := range str {
		if r == '\n' || r == '\r' {
			return string(line)
		}
		line = append(line, r)
	}
	return string(line) // only has one line
}

// SlugifyFirstLine reads the first line of the string and returns a slug of it.
func SlugifyFirstLine(str string) string {
	return Slugify(FirstLine(str))
}
