package kn

// URI is a relative universal identifier. It can be a single Node.Ident or the
// combination of a Node.Ident and a Heading.Ident joined by a hashtag (ex:
// contact#email).
type URI string

// An Index is used to index a cluster of nodes for easy mapping and
// navigation. It marks the top-level node in a cluster and is written to
// KNROOT/index.json. It is composed of data gathered from each index.json
// file for all content nodes which keeps indexing and the entire cluster
// rather fast.
//
// Base contains the base used to construct URIs of any kind. It is usually
// just a domain (ex: skilstak.io) but can include a sub path as well (ex:
// skilstak.io/another/node/cluster).
//
// Nodes contains all alphabetized nodes.
//
// Keywords is an index of all keywords pointing to a list of URIs. The title
// of any URI can be joined from Titles.
//
// Newest contains a list of all nodes sorted from newest to oldest. The title
// can easily be joined in from Titles.
type Index struct {
	Base     string
	Nodes    []Node
	Keywords map[string][]URI
	Titles   map[URI]string
	Newest   []URI
}
