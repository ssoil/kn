package kn_test

import (
	"fmt"
	"gitlab.com/skilstak/soil/kn"
)

func ExampleRemoveSpaces() {
	fmt.Println(kn.RemoveSpaces(`A 😁 Smiley   Section   Header!`))

	// Output:
	//
	// A😁SmileySectionHeader!
}

func ExampleCollapseSpaces() {
	fmt.Println(kn.CollapseSpaces(`A 😁 Smiley   Section   Header!`))

	// Output:
	//
	// A 😁 Smiley Section Header!
}

func ExampleReplaceRune_justspaces() {
	fmt.Println(kn.ReplaceRune(`A 😁 Smiley   Section   Header!`, []rune{' '}, '-'))
	fmt.Println(kn.ReplaceRune(`something simple here?`, []rune{' '}, '-'))
	fmt.Println(kn.ReplaceRune(`something simple here?`, []rune{'?'}, '-'))

	// Output:
	//
	// A-😁-Smiley---Section---Header!
	// something-simple-here?
	// something simple here-
}

func ExampleSlugify_simple() {
	fmt.Println(kn.Slugify(`A 😁 Smiley   Section   Header!`))

	// Output:
	//
	// a-😁-smiley-section-header
}

func ExampleFirstLine() {
	buf := `Here is the first line 😁 huzzah!
	second line
	third line
	`

	fmt.Println(kn.FirstLine(buf))

	// Output:
	//
	// Here is the first line 😁 huzzah!
}

func ExampleSlugifyFirstLine() {
	buf := `Here is the first line 😁 huzzah!
	second line
	third line
	`

	fmt.Println(kn.SlugifyFirstLine(buf))

	// Output:
	//
	// here-is-the-first-line-😁-huzzah
}
