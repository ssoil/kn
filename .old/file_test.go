package kn

import "testing"

func TestPathIsNewer(t *testing.T) {
	path := "testing/newfile"
	old := []string{
		"testing/oldfile",
		"testing/oldfile2",
		"testing/olddir",
		"testing/olddir2",
	}
	if !PathIsNewer(path, old...) {
		t.Errorf("Um, old is stale.")
	}
}
