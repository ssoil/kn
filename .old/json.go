package kn

import (
	"encoding/json"
)

// JSON converts any object to its JSON string equivalent. If there is any
// problem an ERROR key will be created but a string will see be returned.
func JSON(thing interface{}) string {
	byt, err := json.MarshalIndent(thing, "", "  ")
	if err != nil {
		return ""
	}
	return string(byt)
}
