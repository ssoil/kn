package kn

import "testing"

func arraysMatch(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, n := range b {
		if a[i] != n {
			return false
		}
	}
	return true
}

func TestCluster_ListNodes(t *testing.T) {
	c := LocalCluster{}
	c.Root = "testing/cluster-list-nodes"
	nodes := c.ListNodes()
	should := []string{
		"a-good-one",
	}
	if !arraysMatch(nodes, should) {
		t.Errorf("Missed ListNodes: %v != %v", nodes, should)
	}
}

func TestCluster_NodesNeedBuild_all(t *testing.T) {
	cluster := LocalCluster{}
	cluster.Root = "testing/cluster-nodes-need-build"
	nodes := cluster.NodesNeedBuild()
	should := []string{
		"node-updated-meta",
		"node-updated-readme",
	}
	if len(nodes) != len(should) {
		t.Errorf("Missed NodesNeedBuild: %v != %v", nodes, should)
	}
	for i, n := range nodes {
		if should[i] != n {
			t.Errorf("Missed NodesNeedBuild: %v != %v", nodes, should)
		}
	}
}

func TestCluster_NodesNeedBuild_meta(t *testing.T) {
	cluster := LocalCluster{}
	cluster.Root = "testing/cluster-nodes-need-build"
	nodes := cluster.NodesNeedBuild("node-does-not-need", "node-updated-meta")
	should := []string{
		"node-updated-meta",
	}
	if len(nodes) != len(should) {
		t.Errorf("Missed NodesNeedBuild: %v != %v", nodes, should)
	}
	for i, n := range nodes {
		if should[i] != n {
			t.Errorf("Missed NodesNeedBuild: %v != %v", nodes, should)
		}
	}
}

func TestCluster_NodesNeedBuild_readme(t *testing.T) {
	cluster := LocalCluster{}
	cluster.Root = "testing/cluster-nodes-need-build"
	nodes := cluster.NodesNeedBuild("node-does-not-need", "node-updated-readme")
	should := []string{
		"node-updated-readme",
	}
	if len(nodes) != len(should) {
		t.Errorf("Missed NodesNeedBuild: %v != %v", nodes, should)
	}
	for i, n := range nodes {
		if should[i] != n {
			t.Errorf("Missed NodesNeedBuild: %v != %v", nodes, should)
		}
	}
}
