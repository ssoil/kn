package kn

import (
	"fmt"
	"testing"
)

func ExampleParseHeadings() {

	headings := ParseHeadings("testing/headings.md")
	fmt.Println(headings)

	// Output:
	// [{
	//   "Text": "Here is a Title",
	//   "Level": 1,
	//   "Ident": "here-is-a-title"
	// } {
	//   "Text": "Here is another title.",
	//   "Level": 2,
	//   "Ident": "here-is-another-title."
	// } {
	//   "Text": "Third level",
	//   "Level": 3,
	//   "Ident": "third-level"
	// } {
	//   "Text": "Fourth level",
	//   "Level": 4,
	//   "Ident": "fourth-level"
	// } {
	//   "Text": "Fifth level",
	//   "Level": 5,
	//   "Ident": "fifth-level"
	// } {
	//   "Text": "Sixth level",
	//   "Level": 6,
	//   "Ident": "sixth-level"
	// }]

}

func TestParseHeadings(t *testing.T) {
	headings := ParseHeadings("testing/headings.md")
	if len(headings) != 6 {
		t.Errorf("missed a header somewhere or got too many: %v", len(headings))
	}
	if headings[0].Text != "Here is a Title" {
		t.Error("failed to get first header")
	}
	if headings[0].Level != 1 {
		t.Error("wrong header level for 1")
	}
	if headings[1].Text != "Here is another title." {
		t.Error("failed to get the second header")
	}
	if headings[1].Level != 2 {
		t.Error("wrong header level for 2")
	}
	if headings[2].Level != 3 {
		t.Error("wrong header level for 3")
	}
	if headings[3].Level != 4 {
		t.Error("wrong header level for 4")
	}
	if headings[4].Level != 5 {
		t.Error("wrong header level for 5")
	}
	if headings[5].Level != 6 {
		t.Error("wrong header level for 6")
	}
}
