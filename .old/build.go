package kn

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

// Build creates an index.html file from the README.md file in the
// given path directory using the pandoc command. Any meta.yml file found in
// the KNROOT environment variable as well as any meta.yml file in path will
// also be added (with the local meta.yml having priority). This allows all the
// power of template includes since any field in the meta.yml files that
// contains text will be assumed to be Markdown to be rendered. Therefore
// including a snippet in everything is as easy as changing it in the
// KNROOT/template.html file or adding it to KNROOT/meta.yml.
//
// $title$ will be set from the first heading found in the README.md by
// ParseHeadings.
//
// $lastmod$ will be set from examining the file.
//
// $epoch$ is set with the seconds since the UNIX epoch to allow JavaScript
// age since the update to be calculated and displayed in real time.
//
// Build also creates a data.json output file containing all information from
// the meta.yml file as well an index of all headings, with their levels, and
// slugs. This file is used to index the entire site (cluster of knowledge
// nodes) by simply combining them ensuring the least amount of redundant index
// parsing. This also provides a very reliable way for any external application
// to index or consume this meta data.
func Build(path string) Node {
	fpath := filepath.Join(path, "README.md")
	headings := ParseHeadings(fpath)
	if headings[0].Level != 1 {
		Exit("Invalid title header (must be single #): " + headings[0].Text)
	}
	title := headings[0].Text
	info, _ := os.Stat(path)
	mt := info.ModTime()
	lastmod := mt.Format("Monday, January 2, 2006 - 3:04:05pm")
	epoch := fmt.Sprintf("%v", mt.Unix())
	knroot := os.Getenv("KNROOT")
	maindata := filepath.Join(knroot, "meta.yml")
	locdata := filepath.Join(path, "meta.yml")
	args := []string{"pandoc"}
	if FileExists(maindata) {
		args = append(args, maindata)
	}
	if FileExists(locdata) {
		args = append(args, locdata)
	}
	args = append(args, fpath, "-s",
		"-M", "title="+title,
		"-M", "lastmod="+lastmod,
		"-M", "epoch="+epoch,
		"-o", filepath.Join(path, "index.html"),
		"--template="+filepath.Join(knroot, "template.html"),
	)
	// TODO create the index.json file
	cmd := exec.Command(args[0], args[1:]...)
	err := cmd.Run()
	if err != nil {
		Exit(err)
	}
	// TODO populate
	return Node{}
}
