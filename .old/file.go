package kn

import (
	"os"
)

// FileExists simply checks that a file or directory exists.
func FileExists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

// PathIsNewer checks if the file of directory at location path is newer than
// all the others. If the path does not exist or cannot be Stated for info it
// returns false. If one of the others doesn't exist or cannot be Stated and
// path does exist it will count as path being newer.
func PathIsNewer(path string, others ...string) bool {
	i, err := os.Stat(path)
	if err != nil {
		return false
	}
	for _, o := range others {
		oi, err := os.Stat(o)
		if err != nil {
			continue
		}
		if oi.ModTime().After(i.ModTime()) {
			return false
		}
	}
	return true
}

// PathIsFresher checks to see if a path has a more recent last modified time
// on it than all the others. If any of the others do not exist they count
// as older. If path doesn't exist or cannot be opened false is returned.
func PathIsFresher(path string, others ...string) bool {
	var err error
	pinfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	ptime := pinfo.ModTime()
	for _, other := range others {
		oinfo, err := os.Stat(other)
		if err != nil {
			continue
		}
		if ptime.Before(oinfo.ModTime()) {
			return false
		}
	}
	return true
}
