package kn

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	fp "path/filepath"
)

// MakeNode builds the node (README.md) inside the specified nodepath for the
// module inferred to be the parent directory of the nodepath. Use MakeNodes()
// when building multiple nodes as one time to avoid the performance hit of
// loading the module data every time.
func MakeNode(nodepath string, d DATA) error {

	// ensure there's a README.md
	readme := fp.Join(nodepath, "README.md")
	info, err := os.Stat(readme)
	if err != nil {
		return err
	}
	modified := info.ModTime().Unix()

	// load local node data over module data
	modreadme := fp.Join(path.Dir(nodepath), "README.md")
	if d == nil {
		d = LoadYAML(modreadme)
	}

	// determine node format, default: article
	d.Add(LoadYAML(readme))
	format := "article"
	if v, has := d["format"]; has {
		format = v.(string)
	}

	// infer the template path from format, create if not exists
	modpath := path.Dir(nodepath)
	dir := fp.Join(modpath, ".kn", "templates")
	os.MkdirAll(dir, 0755)
	template := fp.Join(dir, format+".html")
	if s, _ := os.Stat(template); s == nil {
		ioutil.WriteFile(template, []byte(TEMPLATES[format]), 0600)
	}

	// build the pandoc command
	args := []string{
		`--read=markdown+smart`,
		`--no-highlight`, // TODO read from module configuration
		`--metadata-file=` + modreadme,
		`--metadata=modified=` + fmt.Sprintf("%v", modified),
		`--output=` + fp.Join(nodepath, "index.html"),
		`--standalone`,
		`--template=` + template,
		readme,
	}
	buf, _ := exec.Command("pandoc", args...).CombinedOutput()
	ioutil.WriteFile(
		fp.Join(modpath, ".kn", "buildout.txt"),
		[]byte(fmt.Sprintf("%v\n%v\n", args, string(buf))),
		0600)

	return nil
}

// MakeNodes builds only the specified nodes infering the module of each from
// its path and only loading the module information once. This caching makes
// building nodes most efficient than MakeNode().
func MakeNodes(nodepaths []string) error {

	// lookup the module paths for every nodepath
	mpaths := map[string]bool{}
	for _, np := range nodepaths {
		mpaths[path.Dir(np)] = true
	}

	// preload all the module data
	mdata := map[string]DATA{}
	for mpath, _ := range mpaths {
		mreadme := fp.Join(mpath, "README.md")
		mdata[mpath] = LoadYAML(mreadme)
	}

	// build everything
	for _, np := range nodepaths {
		err := MakeNode(np, mdata[path.Dir(np)])
		if err != nil {
			return err
		}
	}

	return nil
}

// MakeModule builds all the nodes in the module at the given path that
// have a modification time on their README.md file later than the one
// recorded in the kndex.json file. Other file updates within any node
// will not trigger a build.
func MakeModule(mpath string) error {
	mod := LoadModule(mpath)
	d := LoadYAML(fp.Join(mpath, "README.md"))

	for _, np := range NodePaths(mpath) {
		readme := fp.Join(np, "README.md")
		i, _ := os.Stat(readme)
		if i.ModTime().Unix() <= mod.Modified {
			continue
		}
		err := MakeNode(np, d)
		if err != nil {
			return err
		}
	}

	MakeIndex(mpath)
	return nil
}
