# KN^3 Command Interface (kn)

* Assumes proficiency with `vim` -- preferably with [this](https://gitlab.com/skilstak/config/vim) configuration.

* Nodes can be clustered together with the root containing a `.kn` directory.

* Only top-level directories are considered content nodes. If a `README.md` file in such a node links to a subdirectory in any way that sub-content will not be included in any indexing in any way, even if such a subdirectory contains a `README.md` file. The goal and requirement is a perfectly flat directory structure with interconnected nodes through hyperlinking.

* Only directories with a `README.md` are considered *knowledge nodes*. The rest are just asset directories.

* Main `README.md` of the root node in a cluster is the title/facing page with no indexable content.

* YAML is only for humanly maintained data (`data.yml`).

* JSON is for redundant data used by computers (`index.json`).

* *Nothing* should be in a knowledge content node directory other than the raw content. Including anything else unnecessarily burdens the node with specific rendering file that have nothing to do with the actual content. Doing so is directly against KN^3 best practices and can result in nodes being flagged. 

* Any meta data about the node should either be included in the `README.md` file or the `data.yml` file. In other words, no unused data should ever be in any content node at any time. This prevents the slippery slope of TODOs and forgotten files.

* Javascript and other rendering specific code should be either externally referenced from the pandoc template or embedded in it.

* Content nodes can be combined differently to create different stand-alone publications and sites.

* Runnable from crontab

## Pandoc Markdown

Pandoc is a spectacular achievement and mandatory tool for any knowledge writer.

Although everything from Pandoc Markdown is supported it's a good idea to use the least complicated form of Markdown possible. I recommend EzMark, which is 100% compatible with minimal blogging services and other basic support for Markdown (Reddit, Discord, StackExchange, etc.) Obviously this will not always be possible -- especially when creating knowledge content about science and math that requires Pandoc math syntax based on industry standards. 

The following, however, are *not* allowed and disabled when possible:

* paragraphs must be single lines (better editor wrapping)
* all headers must be ATX style
* no space allowed before header (standard Markdown)
* code block syntax highlighting disabled (use javascript based)

## Markdown Constraints

In order to keep knowledge content source easy to write, parse, and analyze -- even from simple shell scripts -- `README.md` files have a few extra constraints:

* only ATX style headers (one line)
* first header in `README.md` must be level 1 (`# `)
* first header must be on first line of `README.md`
* no skipping header levels
* code blocks must begin with three backticks or tildes *
* headers must never contain links
* headers will be converted to clickable links (filling paste buffer)

* One main reason for these constraints is to disambiguate comments in source code blocks from actual headers. Pandoc markdown already requires a blank line before a header (where many Markdown versions do not).

## Why Bash?

Because it is the fastest way to [duct tape prototype](https://skilstak.io/duct-tape-coding/) this thing --- which has already saved countless hours. Bash is a ridiculously better language that Python for this sort of stuff and *any* strict typing that might be needed can be encapsulated in a single executable in whatever language is best for the job. This allows using the following best-of-breed tools together and maximizing the UNIX philosophy approach:

* `pandoc` - Haskell
* `browser-sync` - Node.js
* `jq` - C
* `yq` - Python2 (which I'm porting to Rust)
* `slug` - Go
* `muffet` - Go
* `vim` - C
* `git` - C, Perl

Say what you want about Bash, but it is objectively the best approach at taping together these other amazing tools in a way that is as powerful and agile as is required to adapt to my ongoing changing needs as new approaches and business processes are discovered.

## Contact Me

If you have any questions or suggestions I'm happy to discuss them. Email <rwx@robs.io>.
