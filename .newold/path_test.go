package kn

import (
	"os"
	"testing"
)

func TestPath(t *testing.T) {
	t.Log(Path())
	os.Setenv("KNPATH", "/some/path")
	t.Log(Path())
	os.Setenv("KNPATH", "/some/path:/to/another:/thing/")
	t.Log(Path())
	// TODO test it for real
}

func TestLocalModules(t *testing.T) {
	//os.Unsetenv("KNPATH")
	os.Setenv("KNPATH", "/home/rob/repos/skilstak/learn")
	t.Log(LocalModules())
	// TODO test it for real
}

func TestLocalNodes(t *testing.T) {
	os.Setenv("KNPATH", "/home/rob/repos/skilstak/learn")
	t.Log(LocalNodes())
	// TODO test it for real
}
