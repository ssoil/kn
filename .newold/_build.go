package kn

import (
	"fmt"
	"os"
	fp "path/filepath"

	"gitlab.com/skilstak/go/pandoc"
)

// Build builds one or more nodes creating an index.html file for each. Specify them by their identities
// which must match the exact directory name.
// Note this does not index the knode within the whole site but
// does update the local knode.json used for site indexing.
//
// The only file required is README.md.
//
// Build derives a few Pandoc template variables:
//
// * $title$ will be set from the first line in the README.md.
//
// * $lastmod$ will be set from examining the file.
//
// * $epoch$ is set with the seconds since the UNIX epoch to allow JavaScript
//   age since the update to be calculated and displayed in real time.
//
// If a <sitedir>/meta.yaml file exists it will be added to the pandoc inputs
// before the README.md
//
// If a local meta.yaml file exists it will be added after the site meta file
// and before the README.md
//
// Note the his means any meta.yaml file must have both a beginning and ending
// triple-dash separator.
func Build(nodes []string) error {
	opt := new(pandoc.Opt)
	site := SiteDir()

	conf := LoadConfig()

	// set the site meta input file if found
	smeta := fp.Join(site, "meta.yaml")
	if _, err := os.Stat(smeta); os.IsNotExist(err) {
		smeta = ""
	}

	// use the default site template if none found in site directory
	// create it in the site directory
	tmpl := fp.Join(site, conf.Template)
	if s, _ := os.Stat(tmpl); s != nil {
		// TODO warn no template detected, using default
		// TODO pull down the template.html if does not exist
		opt.Template = tmpl
	}

	opt.NoHighlight = !conf.Highlight

	// pandoc markdown with smart punctuation
	opt.Reader = "markdown+smart"

	// build each node
	for _, id := range nodes {
		node := fp.Join(site, id)

		// skip if no README.md file (bad node name, no file, etc.)
		// DO NOT CHANGE FROM README.md! It is standard everywhere.
		readme := fp.Join(node, `README.md`)
		if _, err := os.Stat(readme); os.IsNotExist(err) {
			continue
		}

		// set the title
		tline := firstLine(readme)
		if len(tline) < 3 {
			continue
		}
		opt.MetaData = make(map[string]string)
		title := firstLine(readme)[2:]
		opt.MetaData["title"] = title

		// last modified information
		info, _ := os.Stat(readme)
		mt := info.ModTime()
		lastmod := mt.Format(conf.TimeHeading)
		epoch := fmt.Sprintf("%v", mt.Unix())
		opt.MetaData["lastmod"] = lastmod
		opt.MetaData["epoch"] = epoch

		// set the local meta input file if found
		lmeta := fp.Join(node, "meta.yaml")
		if _, err := os.Stat(lmeta); os.IsNotExist(err) {
			lmeta = ""
		}

		// TODO add $canonical$
		// TODO add $summary$
		// TODO add $twitter$ card support
		// TODO add default PWA stuff

		// only add meta input files if they exist
		opt.InputFiles = []string{}
		if smeta != "" {
			opt.InputFiles = append(opt.InputFiles, smeta)
		}
		if lmeta != "" {
			opt.InputFiles = append(opt.InputFiles, lmeta)
		}
		opt.InputFiles = append(opt.InputFiles, readme)

		// output is always index.html
		opt.OutputFile = fp.Join(node, "index.html")

		// TODO add logging mechanism when .kn/config.yaml ready
		//fmt.Printf("%v", opt)

		// TODO write the knode.json file

		//out := pandoc.Exec(opt)
		pandoc.Exec(opt)

		// TODO add logging mechanism when .kn/config.yaml ready
		//fmt.Printf("%v", out)
	}
	return nil
}
