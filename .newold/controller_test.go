package kn

import (
	"testing"
)

func TestController_Init(t *testing.T) {
	t.Log(new(Controller).Init())
}

func TestController_LoadConf(t *testing.T) {
	c := new(Controller).Init()
	c.LoadConf("testdata/blah.json")
	t.Log(c)
}
