# Example of Knowledge Node (KNode) Type Article

This is a sample [Knowledge Node](/knowlege-node/) (knode) of type [article](/knowledge-node-article/). One of six knode [MTRAPD](/mtrapd/) types.

## KNode Article Sections

All articles are composed of one or more *sections* the first of which doubles as information about the article itself. For example, the `Title`, `ID`, and `Summary` apply to the knode and not just the section. This is called "the main section" or "section zero." The main section must be the first thing in the knode article meaning that the file must begin with a single hashtag followed by a space. 

### KNode Article Section Titles and Ids

The first line of a section is always a heading level one which becomes both the `Title` and `Id` of the section and knode after converted into its [auto-identifier](/pandoc-auto-identifers/) (sometimes also called a "slug"). 

> ⚠️ Titles must not contain any markup whatsoever, just raw text.

## KNode Article Section Summary from First Paragraph

The first paragraph is used as the `Summary`. If the paragraph is longer than 250 words only the first 250 words will be used. The words of the summary are second only to the those in the title heading in search weight so make sure to use as many of the keywords needed. This promotes good writing practices in general.

## KNode Article Section Word Count

The `Section.WordCount` contains the number of words separated by spaces. Blocks and code fences are omitted from the count. The sum of these counts becomes the `WordCount` for the entire knode.

## A Blog By Any Other Name

These days people writing "blog posts" are actually writing articles. True blog entries don't have titles, they have dates and times.

## KNode Article Verbatim Blocks and Code Fences

Blocks

## Sample knode.json File

Here's what the `knode.json` file would look like for this document:

```json
{
  "Type": "Article",
  "WordCount": 600,
  "Sections": [
    {
      "Title": "Example of Knowledge Node (KNode) Article",
      "ID": "example-of-knowledge-node-knode-article",
      "Summary": "This is a sample Knowledge Node (knode) article. One of five knode MTRAP types.",
    },
    {
      "Title": "Summary from First Paragraph",
      "ID": "summary-from-first-paragraph",
    }
  ]
}
```


