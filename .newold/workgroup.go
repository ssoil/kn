package kn

import "sync"

// AsyncJob must be fulfilled to send jobs to a WorkGroup. Note that the
// implementation of the Job() method must be completely safe for concurrency.
type AsyncJob interface {
	AsyncJob()
}

// AsyncWorkGroup encapsulates a channel of AsyncJobs with one or more goroutine and
// sync.WaitGroup for them all. See NewAsyncWorkGroup().
type AsyncWorkGroup struct {
	Jobs chan AsyncJob
	wait sync.WaitGroup
}

// Close closes the Jobs channel and waits for all the goroutines to finish.
func (g *AsyncWorkGroup) Close() {
	close(g.Jobs)
	g.wait.Wait()
}

// NewAsyncWorkGroup creates an AsyncWorkGroup with count number of goroutines all
// concurrently reading from the same Jobs channel for work to do. To send the
// AsyncWorkGroup an AsyncJob send anything that implements the AsyncJob interface
// to the Jobs channel (ex: wg.Jobs <- MyAsyncJob{}). Note that this will block
// until the AsyncWorkGroup has a goroutine freed up to work on it. To avoid
// blocking put this into its own goroutine.
func NewAsyncWorkGroup(count int) *AsyncWorkGroup {
	g := new(AsyncWorkGroup)
	g.Jobs = make(chan AsyncJob)
	g.wait.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			for job := range g.Jobs {
				job.AsyncJob()
			}
		}()
		g.wait.Done()
	}
	return g
}
