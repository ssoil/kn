package kn

import (
	"bufio"
	"io"
)

// Heading is a heading encountered in a Node Markdown file.
type Heading []interface{}

// String fulfills the Stringer interface as a simple JSON array
func (o Heading) String() string { return ConvertToJSON(o) }

// ParseHeadings parses all Pandoc Markdown ATX-style headings (initial
// hashtags) from the provided source. Returns error if not ATX or
// not 1-6 hashtags followed by a single space. Each heading must
// be on a single line.
func ParseHeadings(r io.Reader) []Heading {
	hs := []Heading{}

	// states
	inCode := false
	fence := ""
	fencel := 0
	wasBlank := true
	inYAML := false

	s := bufio.NewScanner(r)
	for s.Scan() {
		line := s.Text()
		n := len(line)

		if n == 0 || (n > 0 && line[0] == ' ') {
			wasBlank = true
			continue
		}

		switch {
		case !wasBlank:
			continue
		case inYAML:
			if n == 3 && line == "---" {
				inYAML = false
			}
			continue
		case inCode:
			if n >= fencel && line[0:fencel] == fence {
				inCode = false
				fence = ""
				fencel = 0
			}
			continue
		case n >= 3 && line[0:3] == "## ":
			md := line[3:]
			hs = append(hs, Heading{2, AutoIdent(md), md})
		case n >= 4 && line[0:4] == "### ":
			md := line[4:]
			hs = append(hs, Heading{3, AutoIdent(md), md})
		case n >= 5 && line[0:5] == "#### ":
			md := line[5:]
			hs = append(hs, Heading{4, AutoIdent(md), md})
		case n >= 6 && line[0:6] == "##### ":
			md := line[6:]
			hs = append(hs, Heading{5, AutoIdent(md), md})
		case n >= 7 && line[0:7] == "###### ":
			md := line[7:]
			hs = append(hs, Heading{6, AutoIdent(md), md})
		case n >= 2 && line[0:2] == "# ":
			md := line[2:]
			hs = append(hs, Heading{1, AutoIdent(md), md})
		case n == 3 && line == "---":
			inYAML = true
			continue
		case n >= 4 && line[0:4] == "~~~~":
			inCode = true
			fence = "~~~~"
			fencel = 4
			continue
		case n >= 3 && line[0:3] == "~~~":
			inCode = true
			fence = "~~~"
			fencel = 3
			continue
		case n >= 3 && line[0:3] == "```":
			inCode = true
			fence = "```"
			fencel = 3
			continue
		}
		wasBlank = false
	}

	return hs
}
