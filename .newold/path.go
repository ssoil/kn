package kn

import (
	"io/ioutil"
	"os"
	"path"
	fp "path/filepath"
	"strings"
)

// Path returns a slice of strings containing the filesystem
// directory paths containing module subdirectories. By detault
// this is the grandparent of the current working directory (assuming
// being called from within a specific node subdirectory of a specific
// module). If a kn directory is found the current user's home directory
// it will be used instead. If a KNPATH environment variable is set
// each directory path separated by colons will be used instead.
func Path() []string {

	cwd, _ := os.Getwd()
	gp := path.Dir(path.Dir(cwd))

	paths := []string{gp}

	// by default set to ~/kn
	knd := fp.Join(UserHome(), "kn")
	if _, err := os.Stat(knd); err == nil {
		paths[0] = knd
	}

	// override if KNPATH set
	knpath := os.Getenv("KNPATH")
	if knpath != "" {
		paths = strings.Split(os.Getenv("KNPATH"), ":")
	}

	return paths
}

// LocalModules returns the paths to all module subdirectories found on the
// local system. Module subdirectories must have a kmod.json file to be
// recognized even if the file is empty. Also see Path.
func LocalModules() []string {
	mods := []string{}
	for _, pdir := range Path() {
		inodes, err := ioutil.ReadDir(pdir)
		if err != nil {
			continue
		}
		for _, inode := range inodes {
			if inode.IsDir() {
				mdir := fp.Join(pdir, inode.Name())
				if _, err := os.Stat(fp.Join(mdir, "kmod.json")); err == nil {
					mods = append(mods, mdir)
				}
			}
		}
	}
	return mods
}

//LocalNodes returns the full paths to all local nodes found in all local
//modules. Each node directory must contain a README.md file or it will be
//ignored. The module can be implied from the parent directory of each node
//directory (node.Dir()). Also see LocalModules.
func LocalNodes() []string {
	nodes := []string{}
	for _, mdir := range LocalModules() {
		inodes, err := ioutil.ReadDir(mdir)
		if err != nil {
			continue
		}
		for _, inode := range inodes {
			if inode.IsDir() {
				nd := fp.Join(mdir, inode.Name())
				if _, err := os.Stat(fp.Join(nd, "README.md")); err == nil {
					nodes = append(nodes, nd)
				}
			}
		}
	}
	return nodes
}
