package kn

import (
	"fmt"
)

// PandocJob encapsulates a single Pandoc build command for a specific module.
type PandocBuildJob struct {
	path string
	mod  *ModManager
	//TODO
}

// Job fulfills the Job interface.
func (j PandocBuildJob) AsyncJob() {
	fmt.Println("Would build " + j.path)
	fmt.Println("Would send ModEvent event")
}
