package kn

import (
	"fmt"
	"os"
	"testing"
)

func ExampleParseHeadings() {

	file, _ := os.Open("testdata/knode.md")
	headings := ParseHeadings(file)
	fmt.Println(headings)

	// Output:
	// [[
	//   1,
	//   "here-is-a-title",
	//   "Here is a Title"
	// ] [
	//   2,
	//   "here-is-another-title",
	//   "Here is Another Title"
	// ] [
	//   2,
	//   "another-second",
	//   "Another Second"
	// ] [
	//   3,
	//   "third-level",
	//   "Third level"
	// ] [
	//   4,
	//   "fourth-level",
	//   "Fourth level"
	// ] [
	//   5,
	//   "fifth-level",
	//   "Fifth level"
	// ] [
	//   6,
	//   "sixth-level",
	//   "Sixth level"
	// ]]

}

func TestParseHeadings(t *testing.T) {
	file, _ := os.Open("testdata/knode.md")
	headings := ParseHeadings(file)
	t.Log(headings)
	return
	if len(headings) != 6 {
		t.Errorf("missed a header somewhere or got too many: %v", len(headings))
	}
	if headings[0][2] != "Here is a Title" {
		t.Error("failed to get first header")
	}
	if headings[0][0] != 1 {
		t.Error("wrong header level for 1")
	}
	if headings[1][2] != "Here is another title." {
		t.Error("failed to get the second header")
	}
	if headings[1][2] != 2 {
		t.Error("wrong header level for 2")
	}
	if headings[2][0] != 3 {
		t.Error("wrong header level for 3")
	}
	if headings[3][0] != 4 {
		t.Error("wrong header level for 4")
	}
	if headings[4][0] != 5 {
		t.Error("wrong header level for 5")
	}
	if headings[5][0] != 6 {
		t.Error("wrong header level for 6")
	}
}
