package kn

import (
	"bufio"
	"github.com/ghodss/yaml"
	"io"
)

// ParseFrontMatter parses YAML front matter and returns. Note YAML
// can only be placed at the very beginning of a content node and
// must begin on the first line and must end with --- (not ...).
func ParseFrontMatter(r io.Reader) DATA {
	yml := map[string]interface{}{}
	s := bufio.NewScanner(r)
	s.Scan()
	line := s.Text()
	buf := ""
	if !(len(line) == 3 && line[0:3] == "---") {
		return yml
	}
	buf += "---\n"
	for s.Scan() {
		line := s.Text()
		if len(line) == 3 && line == "---" {
			break
		}
		buf += line + "\n"
	}
	yaml.Unmarshal([]byte(buf), &yml)
	return yml
}
