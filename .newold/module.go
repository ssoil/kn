package kn

import "sync"

type ModReqType int

const (
	Build ModReqType = iota + 1
	Close
)

// ModRequest is a request send to the ModMgr to perform an action.
type ModRequest struct {
	Type ModReqType
	Data interface{}
}

// ModManager safely and concurrently manages the state and kmod.json file for
// a given module by allowing sychronous events to be sent to it over its
// Requests channel.
type ModManager struct {
	Requests chan ModRequest
	data     DATA
	wg       sync.WaitGroup
}

// Open opens and reads the JSON data from path file, opens the Events channel,
// and starts a goroutine that only responds to incoming ModRequests until the
// reguests channel is closed.
func (m *ModManager) Open(path string) {
	// TODO change to a file open that reads all the data without losing the file descriptor so that Seek(0,0) will allow us to update the data more quickly so long as the ModManger alive and accepting requests.
	m.data = LoadJSON(path)
	m.Requests = make(chan ModRequest)
	m.wg.Add(1)
	go func() {
		for ev := range m.Requests {
			dump(ev)
			// TODO do something with the event
		}
		m.wg.Done()
	}()
	m.wg.Wait()
}
