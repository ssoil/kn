package kn

import (
	"encoding/json"
	"io/ioutil"
)

// DATA is just a bucket of untyped data usually unmarshalled from JSON or
// YAML. This is useful when the exact structure of the data is not known
// or fluid.
type DATA map[string]interface{}

// Save dumps the DATA as human readable JSON file at the specified path
// returning any error. File is read and write by current user only.
func (d DATA) Save(path string) error {
	return ioutil.WriteFile(path, []byte(d.String()), 0600)
}

// String fulfills the Stringer interface as JSON
func (d DATA) String() string { return ConvertToJSON(d) }

// ParseJSON reads bytes of JSON from and returns a DATA object.
func ParseJSON(byt []byte) DATA {
	data := DATA{}
	json.Unmarshal(byt, &data)
	return data
}

// LoadJSON loads a file containing JSON data specified by path into a DATA
// object. Returns empty DATA if unable to find or open the file.
func LoadJSON(path string) DATA {
	byt, _ := ioutil.ReadFile(path)
	return ParseJSON(byt)
}
