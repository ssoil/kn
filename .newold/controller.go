package kn

import (
	"encoding/json"
	"io/ioutil"
	"os"
	fp "path/filepath"
	"runtime"
)

// Controller is the workhorse of this package. Each can be thought of as
// a separate run-time. Each carries its own configuration and can do it's work
// independently from and concurrently with other Controllers even within the
// same appliction. Defaults are noted in the comments but are set by Init().
//
// Note that many of these properties can be set per module (in the kmod.json
// file) and even per node (in the YAML frontmatter)
//
// Note the kn command bundled with this package simply wraps a
// single Controller.
type Controller struct {

	// Use context highlighting or not by default.
	// (Default: false)
	Highlight bool

	// The template file to look for in each module root
	// (Default: template.html)
	TemplateFile string

	// The data format to use for time based headings used both printing
	// and parsing nodes of format type log.
	// (Default: "Monday, January 2, 2006, 3:04:05PM")
	TimeHeading string

	// The number of builder goroutines to startup and maintain.
	// (Default: number of cores on system)
	BuilderCount int

	// Each logical module found with ModEvent messaging channel.
	Mod map[string]*ModManager

	// Pipe in jobs to get the done concurrently.
	Build *AsyncWorkGroup
}

// String fulfills the String interface as JSON output.
func (c Controller) String() string { return ConvertToJSON(c) }

// Init initializes a Controller setting defaults and loads any detected
// configuration in the directory pointed to by the KNCONF environment
// variable from the .knconf.json file in current user's home directory.
func (c *Controller) Init() *Controller {

	// set defaults
	c.Highlight = false
	c.TemplateFile = "template.html"
	c.TimeHeading = "Monday, January 2, 2006, 3:04:05PM"
	c.BuilderCount = runtime.NumCPU()

	// look for config file
	path := os.Getenv("KNCONF")
	if path == "" {
		path = fp.Join(UserHome(), ".knconf.json")
	}
	c.LoadConf(path)

	// startup mod managers
	// TODO for each module add

	// create a work group for async build jobs
	c.Build = NewAsyncWorkGroup(c.BuilderCount)
	// c.Build <- PandocBuildJob{"blah"}

	return c
}

// LoadConf loads a configuration file from a specific path.
func (c *Controller) LoadConf(path string) {
	byt, _ := ioutil.ReadFile(path)
	json.Unmarshal(byt, c)
	return
}
