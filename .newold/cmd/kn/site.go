package main

import (
	"fmt"

	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/skilstak/soil/kn"
)

func init() {
	site := cmd.New("site")
	site.Summary = `print path to current site content directory`
	site.Method = func(args []string) error {
		fmt.Println(kn.SiteDir())
		return nil
	}
}
