package main

import (
	"fmt"

	"gitlab.com/skilstak/go/cmd"
)

// extra semicolons for safety depending on how users add to their bashrc
const shell = `
export _kn=$(which kn);

kn () {
  case "$1" in 
    d) shift;
       mdir=$($_kn d $*);
       if [[ $? == 0 ]]; then 
         echo $mdir;
         cd "$mdir"
       fi
       ;;
    *) $_kn $* ;; 
  esac;
};

export -f kn;
complete -C kn kn;
`

func init() {
	bashrc := cmd.New("bashrc")
	bashrc.Summary = "print the recommended lines to add to a bashrc"
	bashrc.Usage = ""
	bashrc.Description = `
            Prints the lines to add to a bashrc (or simply eval) providing the following:

            * Adds the *kn* alias
            * Adds tab completion for *kn*
      `
	bashrc.Method = func(args []string) error {
		fmt.Println(shell)
		return nil
	}
}
