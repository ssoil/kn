package main

import (
	"fmt"
	"path/filepath"

	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/skilstak/soil/kn"
)

func init() {
	d := cmd.New("d")
	d.Summary = "opens directory for the given knode"
	d.Usage = "<knode>"
	d.Description = `
            Prints the directory matched from arguments. If called from the *kn*
            bashrc function also changes into the directory.
            
            If no argument is provided prints (and changes) into the *$KNSITE*
            directory if set.
      `
	d.Method = func(args []string) error {
		site := kn.SiteDir()
		if len(args) == 0 {
			fmt.Println(site)
			return nil
		}
		// TODO add completion based on knode ids
		fmt.Println(filepath.Join(site, args[0]))
		return nil
	}
}
