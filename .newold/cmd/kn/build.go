package main

import (
	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/skilstak/soil/kn"
)

func init() {
	build := cmd.New("build")
	build.Summary = `build site or one or more nodes`
	build.Usage = `[<nodeid> ...]`
	build.Method = func(args []string) error {
		return kn.Build(args)
	}
}
