package main

import (
	"gitlab.com/skilstak/go/cmd"
)

func init() {
	kn := cmd.New("kn", "site", "build", "bashrc", "d", "id")
	kn.Summary = `make and manage SOIL knowledge node content`
	kn.Version = `v0.0.1`
}
