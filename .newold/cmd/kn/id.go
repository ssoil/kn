package main

import (
	"fmt"
	"strings"

	"gitlab.com/skilstak/go/cmd"
	"gitlab.com/skilstak/go/pandoc"
)

func init() {
	id := cmd.New("id")
	id.Summary = `convert text to Pandoc auto-identifier`
	id.Usage = `text`
	id.Method = func(args []string) error {
		text := strings.Join(args, " ")
		fmt.Println(pandoc.AutoIdent(text))
		return nil
	}
}
