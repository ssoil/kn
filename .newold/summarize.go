package kn

import (
	"os"
	"path"
	fp "path/filepath"
)

// SummarizeNode summarizes the specified node at path by parsing the
// frontmatter YAML data and headings and returning as JSON DATA. Does not
// validate.  Returns empty data if not found or does not contain README.md
// file.
func SummarizeNode(p string) DATA {

	// infer the ID and readme
	id := path.Base(p)
	file := fp.Join(p, "README.md")

	// parse the yaml frontmatter from it
	readme, _ := os.Open(file)
	defer readme.Close()
	data := ParseFrontMatter(readme)

	// parse the headings
	readme.Seek(0, 0)
	headings := ParseHeadings(readme)

	// always consider the title the first level one heading
	if val, has := data["title"]; has {
		headings = append([]Heading{{id, val.(string), 1}}, headings...)
	}
	data["headings"] = headings

	// apply authoratative overrides
	data["id"] = id

	return data
}
