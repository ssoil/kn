package kn

import (
	"fmt"
	"os/user"
)

func dump(thing interface{}) {
	fmt.Printf("%v\n", thing)
}

// UserHome returns the full path to the current user's home directory.
func UserHome() string {
	u, _ := user.Current()
	return u.HomeDir
}
