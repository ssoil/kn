# SOIL Knowledge Node Static Site Generator in Go (golang)

> ☕ Currently under construction (porting from [old bash](.oldbash). Actively seeking help with specific needs. See issue tracker for work that needs to be done.

The `kn` static site generator tool uses the amazing and widely used `pandoc` tool to provide cross-platform content generation and management using the full power of Pandoc Markdown combined with the unrivaled utility of the Go language. Unlike other static site generators `kn` is focused on creating and promoting and interlinking educational resources and documentation that universally portable and maintainable. Content created this way can easily be converted to PDF, Kindle, or even a full textbook. The key is modularity and flattened content into individual content nodes which can be combined and interlinked without affecting the source content. This keeps both node content and URLs short and relevant which is proven to increase SEO rankings. It also encourages rendering speed and inbound linking further promoting SEO.

## Why GPLv3 License?

First of all, this license has nothing to do with any site or content you create or sell using the `kn` tool. It only applies to companies that would otherwise take the code of the tool, put it into a commercial product, and sell it without giving back any of their changes (like Apple and some edtech companies that find no ethical problem doing this). GPLv3 legally blocks this greedy behavior protecting us and promoting a world where people are required to share improvements to things others first shared with them.

> 💢 Companies like Apple have proven that when given the chance they will literally steal as much free "open source" code that they can (taking them to a trillion dollar valuation) *without giving a single thing back to the community.* That is *exactly* what "Darwin" is, a stolen BSD-based operating system that saved Apple millions of dollars that it would otherwise have to pay to replace the completely broken original operating system. Apple has hidden this fact in violation of the terms even of the BSD license itself, but no one has yet had the courage to legally force them to comply. Not surprising from a company that deliberately forces you to throw your devices into a landfill rather than replacing your battery.

## Old Bash Version

I have kept the old [bash](.oldbash) version for reference for those wishing to implement their own in shell rather than Go.

## Anatomy of a Knowledge Node

Knowledge Nodes are designed to be the simplest possible structured writing to enable anyone to create them easily.

### Directory with `README.md` with Pandoc Auto-Identifer Title

Each Knowledge Node, or knode, is contained within a directory with a dashed ID name that matches the Node.ID value. The Pandoc Auto-Identifier algorithm is used to derive the ID from the first-line, first-level header title in the required `README.md` file within each knode directory.

The `README.md` is written in Simplified Pandoc Markdown and allows the inclusion of "front matter" YAML to be used by renderers such as Pandoc and other tools.

Everything else builds on this base.

### Other Files

Other content files such as images, sound, and files of other file types can always be included within the same knode directory. Each should be referred to by relative path from the `README.md` file. No other markup files should be included, however, since they are better managed as their own knodes.

When bundling a knode for inclusion in a ksite bundle or otherwise *every* file within the knode directory will be added including dotted and other directories and files that are generally considered hidden. Therefore, avoid putting anything in a knode that is not relevant to the content.

To help avoid creation of unused and orphan content (which can quickly bloat a knode and its ksite) any content within a knode directory that is *not* linked from the `README.md` will be flagged and cause a fatal build error when using the `kn` tool.

## Anatomy of a KnowledgeNet Site

An individual `LICENSE` file may also be included but if not these are assumed to be that of the site itself (in the parent directory).

If those look a lot like what is included in most software project repos that's because the KnowledgeNet architecture is modeled as closely as possible to open source software development model. Knowledge Nodes are just *knowledge* source files packaged as modules that have dependencies on others exactly like software.

## Pandoc Command

The `pandoc` command executed for each *knowledge node* roughly as follows executed from within the knode directory:

```sh
pandoc \
  --standalone \
  --writer=markdown+smart \
  --template=$KNWORK/template.html \
  --output=index.html \
  --no-highlight \
  $KNWORK/meta.yaml \
  meta.yaml \
  README.md 
```

Only the `--no-highlight` can be disabled through configuration. The rest constitutes the files and consistent format of a standard *knowledge node*.

Note that both the `$KNWORK/meta.yaml` and `meta.yaml` files will need begin and end with `---` in order to work.

It is also worth mentioning that only `$KNWORK/template.html` may contain Pandoc template markup. Specifically, such markup is not permitted within the `README.md` file (\$stuff\$ is regarded as Math markup).

## Simplified Pandoc Markdown

Pandoc Markdown is without question the most powerful markup currently available and meets the demands of both content developers working with *any* output format.

However, Pandoc Markdown propagates some complexities in the name of general Markdown support that simply do not make sense and unfortunately incur a great deal of overhead both when creating content as well as parsing it.

Observing the following required knode content simplifications will not only make it easier to write and maintain content but also make it easier to share on services like Medium and analyze and manipulate with even the most rudimentary shell scripts.

Most of these changes are based on prioritizing the principle of *consistency* over artistic expression in the content source code, "one best way," in other words. For example, there is absolutely no need for an infinite number of ways to indicate a content separation (horizontal rule). One way is better to find,filter, and parse.

### HTML Should Always Be Secondary

The great thing about Pandoc is that is was not created specifically to convert Markdown into HTML. It is designed to be an easy,unique, structured writing format. A sort of coding language for knowledge itself. 

Strictly speaking the KnowledgeNet does not depend on the Web nor HTML. KnowledgeNet browsers and readers can be created that don't even use the Web, or even the Internet.

Therefore, HTML, CSS, and JavaScript should *only* be used for secondary content within any Simplified Pandoc Markdown file. For example, someone might at an interactive graph using web tech, but the document should not depend on it. It should instead have simple raster images showing the graph data. If the HTML is removed (as would be by non-Web readers) it should still contain all the valid information.

### Ever Node `README.md` Must Begin with a First-Level Heading 

The first character of the `README.md` file *must* be a hashtag (`#`). This is reminiscent of other script source files that begin with a shebang (`#!`). `README.md` files are *knowledge* source files.

The second character of the `README.md` file *must* be a space.

The third character is the first character of the implied knode title.

This simple convention ensures your content shoots to the top of SEO rankings as well as makes it ridiculous easy to summarize even with simple shell scripting.

### Optional YAML Front Matter

Although Pandoc allows the inclusion of YAML meta data anywhere within a document, only include one such data block at the beginning of the document. It must begin and end with three dashes and the first should always be on the first line. This format for "front matter" is rather universally observed at this point.

The `kn` tool will automatically convey all front matter data into the `knode.json` file under the `Meta` sub-section *except the following* (which are purposefully upper case to show they convey publicly whereas any others are for the local pandoc rendering only):

----------- -----------------------------------
 `Tags`      Becomes the same root element.
 `Updated`   Overrides detection from file.
----------- -----------------------------------

Do *not* add `Authors` and the like. Those belong in the `ksite.json` file.

### Never Use More Than One First-Level Heading

This is a long-standing rule for good SEO but also just makes the best sense. Using the other five heading levels provides a lot of room.

### Use Single Long Lines for Paragraphs

The original Markdown allowed paragraphs to be groups of lines instead of one long one. This was thought to make reading the source easier. It doesn't -- especially today. Content source if frequently crammed into only 20 or 30 columns from TMUX sessions and other resizing. One long line always wraps correctly. Multiple single lines do not producing horrendously unreadable wrapping in all editors with modern defaults set.

### Just Use Stars for Inline Formatting

One star for *italic*, two for **bold**, and three for ***bold italic***. That's it. Don't even mess around with using underscore for any inline formatting. It is a well-known problem for all Markdown variations plus it is horribly inconsistent. Maintaining these single, simple tokens makes parsing source ridiculously easy by comparison.

### Avoid Strike-Through Formatting

Even though this is just a recommendation (and not a `kn` requirement) strike-through very rarely has any practical value. It is also very unreliably rendered and incompatible with services like Medium.

### Never Format Only Part of a Word

There is always a better way to communicate the meaning that making, for example, just the `ing` of `trolling` italics. I realize this makes writing about natural language syntax slightly more difficult, but the result will always be more clear and less likely to become ambiguous when rendered slightly differently.

This also makes parsing and clearing the formatting much easier because the minimal formatting tokens will always be at the beginning or end of a word. If ever a star appears inside of a word it can safely be ignored. However, it is a horrible idea to ever include such characters within words, yes even f\*\*\*. Pandoc wisely requires all of these to be escaped with backslash.

### Only Hashtag Heading Format

Also known as "setex" this format has become the de facto standard because of its ease of use and consistency without loss of clarity. Such headings can quickly be found and filtered because of this consistency, which is simply not possible with the other format.

### Never Use Headings for Formatting

Headings are frequently abused to get a desired style. This is bad on several levels. Just don't do it. Instead using one of the inline formats or use the incredibly useful curly syntax to add a class to some bracketed text.

### Simple Punctuation Throughout

Writing with simple punctuation is simple and efficient. Leave the smart beautifying for the renderer rather than confuse the content.

### Only Inline Links

Only inline links allow the simple cutting and pasting of content containing the link to another location. While the original intent of reference links was to simplify and beautify the source text, such things are no longer necessary -- and downright dangerous today -- given the many Markdown editors that shield us from those rather long URLs and the amount of content editing that happens involving migrating content to different files. The Pandoc Vim plugin is a particularly good example of something that hides the complexity of URLs.

### Use Simple Links to Jump Within a Node

Pandoc supports simply wrapping a link to a heading in the same document by just wrapping it with square brackets. Use this and if that heading ever graduates to its own knode you simply have to add the `(/node-id/)` to it.

### Always Use Trailing Slashes for Local Node Links

This simple and easy to remember convention ensures you minimize the number of redirects and can preview your content in the largest number of ways. 

### Use Four-Space Indent for Content to Be Counted

Poetry and other content that must maintain its spacing (preformatted) should use four spaces rather than fenced blocks because fenced blocks are automatically omitted from word counting and indexing.

### Four-Space Indent for Sub-Lists

Pandoc Markdown requires four initial spaces and four more for every level of
sub-list when creating outline-type lists. Markdown is not specific about this. Always use four-spaces.

### Use Code Fences for Content NOT to Be Counted

Any code fenced content will automatically be omitted from counting and indexing. If you want the content to be included use the four-space indentation rule instead.

### Just Use Triple-Backtick for Fences (Unless Markdown Fenced Content)

Using the triple-backtick fence posts makes code fences consistent to find and filter. There are only two exceptions: when the fence contains Markdown that contains a triple-backtick and when the fence contains Markdown that contains that as well. In these exceptional cases *always* use the triple-tilde or quad-tilde instead.

### Use Contextual Initial Emoji Blockquotes

Although Pandoc supports many different ways to indicate different context for content blocks using simple blockquotes where the first character is an emoji provides a portable, understandable alternative. This was not possible in the original Markdown. Now that UTF-8 is supported in every editor on the planet this is a nice way to distinguish between asides and other contextual content. 

Although Pandoc itself does not support rendering such initial emoji blockquotes (yet) they can be easily supported with a simple JavaScript global script that processes each after it has initially loaded in the web browser.

The `kn` tool can be configured to omit blockquotes of a particular emoji class from indexing.

### Do Not Include Metadata (YAML) in Your Content

Put it in the `meta.yaml` file instead. This file is added to the `README.md` during rendering and use to produce the "knode.json" file. "Front matter" has always been a very dubious design decision for many reasons. Always consider your meta data separate from your actual data.

### Separate Content (Horizontal Rule) with Just Four Dashes

That's right, `----` is all you ever need. That way you can quickly and consistently find and filter them.

### Always Put Images on Own Line

Images are best with a full blank line before and after. The alt text will automatically become the caption with Pandoc. Additional formatting can be added using the curly `{}` syntax Pandoc supports appended directly to the image markup.

### Don't Remotely Source Images

Remotely sources images create unnecessary dependencies, force remote loading to see, and are often violations of copyright and ethics since they appear to be a part of your content but are not. Even dynamically generated images from remote sources are never required despite their popularity on GitLab/GitHub. 

Eventually the `kn` tool will flag remotely source images as syntax errors and refuse to build until thy are corrected.

### Duplicate Images If Necessary

Each knode should generally be as independent as possible -- particularly the content that directly deals with the topic of knode. Obviously it is best to have images only exist in a single knode, but in the rare case where the same image is needed by another it is best to include as well. This prevents text content from becoming unexpectedly out of sync with the image to which it refers. All knode should be updated independently.

This also discourages redundancy in the knowledge source itself promoting a link to the node with the image instead of reusing the image.

### Don't Abuse Lists

Unless you are writing a table of contents (in which case you should consider a kcluster instead) your lists should generally be only on level deep and always use star `*` and `1.` for consistency. Pandoc supports wildly complex lists, however, and one should also feel free to use all this power when needed.

When you think you need an outline consider adding a knode of type *map* instead.

## Design Considerations

Here's a summary of the thinking behind important design decisions as encountered.

### Zero-Configuration Mandate

A content creator must never be required to configure anything to begin using the tool and library. It should just work. All that should be required to get started is the following:

1. Create an appropriately named directory for the module and the first node.
1. Write the required `README.md` file
1. Make, commit, and deploy it.

So the commands would be the following:

```bash
mkdir -p my-module/rob-pike`
cd my-module/rob-pike
vim README.md
kn save
```

If any configuration is missing it will be prompted for interactively and written to files within the `.kn` directory in the user's home directory:

* Ask if GitLab or GitHub or other Git is wanted.
    * Check for Git setup and walk through if not.
* Suggest automatic deployment with Netlify.

#### Environment Variables

The following environment variables allow configuration of the `kn` utility or package:

* `KNPATH` - colon separated list of fully qualified directory paths containing module subdirectories (default: `$HOME/kn`)
* `KNCONF` - fully qualified path to JSON configuration file (default: `$HOME/.knconf.json`)

### Multiple Module Management

Practical experience has proven that most content creators will want to create and maintain more than one KnowledgeNet module at a time --- usually on the same workstation. 

So the default `HOME/kn` directory will automatically be looked for to contain subdirectories for each module. This allows content creators to decide if they want to make each knowledge module its own repo (the usual standard) or make all of the modules into a mono-repo instead.

For those who wish to use another location --- or several different locations --- the `KNPATH` environment variable may be set. Each directory containing module subdirectories is separated by colons (exactly like the `PATH`).

Using either of the two above options is highly recommended.

However, to provide maximum configuration-free ease of use one last implied method will be used if neither of the two main options are detected. If neither `KNPATH` nor `HOME/kn` exists then the grandparent of the current working directory is assumed to contain module subdirectories. This assumes the creator is working from within content directory for specific node within a module directory.

### Progressive Configuration Mandate

The `kn` tool can be used immediately from any directory with nothing but a Pandoc Markdown `README.md` file. In such cases highly optimized defaults will be used such as the default `template.html` file, which will be downloaded if Internet access if available, otherwise it will use the hard-coded internal copy. The `template.html` file will be added automatically when `kn` is first run. The current directory will be assumed to be the site root directory unless its parent has a `.git` directory in which case the current directory will be assumed to be one knowledge node of the site. Additional configuration can be added progressively as needed.

> 💢 One of the biggest annoyances of most static site generation tools is the amount of required boilerplate and configuration files that are needed before you can do anything with it.

### One Template Per Site

The simplicity of only dealing with and maintaining a single simple `template.html` file makes the most sense for the broadest number of use cases. Having a local `template.html` override the site template was considered and discarded, as well as being able to set a template in the local `meta.yaml` data, but this also seemed like a kludge.

The emphasis is on consistency and simplicity with content over style. If content is different enough to warrant an entirely new template it likely does not belong on the same site -- especially give the variety of things that can be done simply with Pandoc Markdown's support for good HTML and inline JavaScript. In fact, Vue can be embedded into individual knodes providing interactivity and non-essential extras. Knowledge nodes, however, should never depend on JavaScript for anything to keep them searchable.

### Inline Formatting in Headings

Most of Pandoc inline formatting in headings is allowed:

* Emphasis with one, two or three stars.
* Strikeout.
* Verbatim with backticks.
* Escaping with backslash.
* Super and Subscripts.
* LaTeX inline math dollars.

The following, however, are not and will cause fatal syntax errors:

* Inline attributes with curly brackets.
* Bracketed identifiers.
* Underscores for emphasis.
* Any HTML at all.
* All forms of linking.

Header attributes (that trail the header in curly brackets) are allowed but
ignored in semantic contexts, such as when not written to the `knode.json` or
`ksite.json` files.

These clarifications are to distinguish the semantic nature of certain inline formatting from stylistic markup. It makes raw headers easier to search and clearer to read in source form.

Indeed, Pandoc Markdown has arrived at the best working standard for semantic markup to convey meaning to text through inline formatting. It is the hope of this project that this standard can receive wide adoption over time.

Even though services like Medium do not allow this, using formatting to change the meaning of text in a heading is frequently used in all languages. Consider the title, "Is *That* What People Do?" Removing the emphasis provides a different connotation. Inline formatting allows for inflection and such that cannot otherwise be conveyed easily. This makes such formatting integral with all written communication.

### No Versioning of Any Kind

At first it seems to makes sense to add a version to each knode since the model is after that of software development. But knowledge source is different. Usually changes to knowledge source need to be ignored and forgotten. Something was incorrect, spelled wrong, harshly worded, offensive, unclear or otherwise unworthy of our remembrance. If not, the authors can include an *Update* in the source itself describing the change. (Medium does not have version tracking either.)

For tracking of individual changes KnowledgeNet authors can opt to include `Git` in their meta data pointing to the Git repository with a full history of all changes.

### Only One License for Site

After considering giving each knode its own license it was decided to just stick with one license for the entire site since the site is what gets saved to an individual Git repository. Anything not under that site license should be in another site.

### Only One Group of Authors for Site

Authorship is important but minimized in each knode. Usually readers don't care who wrote it and when they do can look it up for the entire site and not just the node.

This encourages granularity and decentralization. Individual authors are encouraged to write their own content and interlink with other KNet sites. While co-authorship is certainly allowed it is viewed the same as the contributors to any given open source project, as a list for the whole project. 

Individual changes can be tracked and identified by looking at the Git project repository itself when who did what must be identified.

### Limiting to One Author with Contributors

Wikis have failed, mainly because they allow too many authors. It is always better to have several different, small groups of authors --- perhaps writing about the same thing --- than to force an infinitely large number of authors to agree on what should be written. This is why Wikipedia has largely failed. It fosters mob-mentality v.s. innovative thinking. This is the same reason really bad information is often the most upvoted on StackExchange.

Instead, building a standardized publishing format and API as with KNet allows all these disparate authors and content to be brought together through indexing and searching in *exactly* the same way that software packages and modules that accomplish nearly the same thing co-exist without problem.

### 100% Backwards Compatible Forever

Since there is no compelling reason to ever create breaking changes to the KnowledgeNet model *everything will always be supported*. The API/model/pattern is designed to be so simple it never actually needs any changes. This includes support for *Simplified Pandoc Markdown*.

Note, however, that renderers and browsers *may* render KnowledgeNet differently over time. The source will not have changed though.

This also removes the need to create some sort of KNVersion attribute in the `ksite.json` file.

### Monorepo Support

Putting everything in a single git project repository is popular with some. KnowledgeNet allows for this.

### Nothing But Git

One best tool, as they say. Git is a fundamental requirement of the Linux project and the Go programming language. It is a deeply codified standard on which the KnowledgeNet can reliably and simply depend. There are no plans to ever support any other source repository format. If and when Git ceases being the de facto world standard for source repositories consideration will be given to a replacement.

### Do Not Use Trailing Hashtags in Headings

Even though Markdown and Pandoc Markdown allow the artistic hashtags trailing a heading in order to make it look balanced in source form this is horrendously impractical (and frankly ugly). Just don't do it. Use of such is flagged as an error by the `kn` tool when encountered.

### Inline Links

Since the foundational principle of the KnowledgeNet is treating written content as knowledge source the same as software source it follow that some of the advantages and approaches from software compilation would also apply. Such is the case with *inline links*.

Inline links are simply links that follow a specific format indicating that, when possible, the content located when the link is followed could be placed inline at that location rather than forcing the user to click and follow it. This simple format is as follows:

```markdown

[[*Print Hello World*]](https://skilstak.io/print-hello-world/)␣␣
[[*Policies*]](/print-hello-world/)

```

The above example shows that the text of the link must be surrounded with two brackets and a star. This will appear as a bracketed italicized text when rendered as is standard in professional writing. Multiple inline links can be grouped and separated by using the regular hard break Markdown syntax of two or more spaces.

KnowledgeNet browsers that support inline links can be optionally set to retrieve the information (if it can be inlined) and display it as a titled section with a heading matching the title.

All other browsers will work the same as always making emphasizing where to go to get that content in traditional hyperlinked fashion.

Inlining provides tremendous opportunities for clean content management since several knowledge nodes may depend on another with an aggregation relationship. This prevents redundancy in preparing different content that would otherwise have to be replicated and maintained independently, a serious flaw in almost all printed and online content today. This advantage of the KnowledgeNet promotes much more sustainable and consumable content. 

### No Syntax Highlighting

This is a tough one. Most people seek a static site generator specifically for the ability to make all the code have pretty syntax coloring. There are several problems with this, however, that are regularly ignored:

* *Colors cannot be reliably printed.* The KnowledgeNet is for *everyone* and that includes the 4 billion people without Internet access. These people are more likely to read your content after having had it printed for them. I know this sounds weird to most in the developed world, but even some American school would rather print a lesson (or convert it to PDF) rather than force an Internet connection.

* *Many cannot see color well.* Color blindness is a real thing and even though it doesn't affect you it *will* affect a lot of people that use your content. In America it is illegal to create Web content that is not clear to those with color blindness.

* *Kindle, ePubs, and books don't do color.* If you want your content in digital book form (and you should) you should know that either your readers won't have color (Kindle, for example) or if you do they and you will pay a lot more for it. This is why almost every tech book ever printed on paper uses black and white with no syntax highlighting for code.

* *All that highlighting markup drastically adds to the size of your document.* Sure the Internet is fast, but not for everyone. Besides, is having colored code really worth giving up several 100 spots in Google SEO search ranks? The lightest pages are *always* ranked higher than anything else.

* *Black and white loads faster.* Not only is there less to load. But applying all those colors with CSS or the popular JavaScript method after the page loads are slow. Plus all that CSS and JavaScript have to be loaded well. This can be cached but still if not needed then why.

* *Forces good coding style.* Losing the highlighting forces you to write better, clearer code examples with good white space and alternative ways of bringing attention to a specific part.

* *No color is better for education.* The most common errors people make when learning to code are ignoring the bits of code that are required but often minimized and colored to blend in, like the parentheses, semicolons and other boring parts of the syntax. This de-emphasis works *against* the goals of those learning to code. All code is equal without such opinionated color highlighting.

* *Code can be cut and paste reliably from all source and rendered forms.* This is means you can copy and easily paste the exact content of an element containing code rather than stripping all the context highlighting markup.

### No Line Numbers

Most books written about coding do not have line numbers. Instead they call out specific lines by adding a 1 in a circle or a comment. Getting people to identify locations in the code without first looking at the line number is important to help them focus on the code and not the editor or format of the editor.

## TODO

* Facilitate the maintenance of several modules easily on a single system. Perhaps `$KNPATH`.

* Add default settings in `~` or `~/.config`
