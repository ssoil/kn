package kn

import (
	"fmt"
	"os"
	"os/user"
	fp "path/filepath"
)

// CurMod looks for the KNMOD environment variable and returns that. If
// not found looks for the following in order of priority within the current
// user's home directory: KnowledgeNet, KNet, kn. The idea is that most content
// creators will only have one macro module that they maintain. The module may
// be organized into different maps and such, but the module is a practical
// collection of the knowledge that user maintains and publishes including
// collections composed of others' knowledge nodes maintained in their modules.
func CurMod() string {
	mpath := os.Getenv("KNMOD")
	if len(mpath) > 0 {
		return mpath
	}
	for _, name := range []string{"KnowledgeNet", "KNet", "kn"} {
		knd := fp.Join(home(), name)
		if _, err := os.Stat(knd); err == nil {
			return knd
		}
	}
	return mpath
}

func dump(thing interface{}) {
	fmt.Printf("%v\n", thing)
}

func home() string {
	u, _ := user.Current()
	return u.HomeDir
}
