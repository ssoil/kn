package kn

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// ConvertToJSON converts anything into JSON. Use to implement the Stringer
// interface is a more intuitive, readable way.
func ConvertToJSON(thing interface{}) string {
	byt, err := json.MarshalIndent(thing, "", "  ")
	if err != nil {
		return fmt.Sprintf("{\"ERROR\": \"%v\"}", err)
	}
	return string(byt)
}

// ConvertToJSONTiny converts anything to compact JSON.
func ConvertToJSONTiny(thing interface{}) string {
	byt, err := json.Marshal(thing)
	if err != nil {
		return fmt.Sprintf("{\"ERROR\": \"%v\"}", err)
	}
	return string(byt)
}

// DATA is just a bucket of untyped data usually unmarshalled from JSON or
// YAML. This is useful when the exact structure of the data is not known
// or fluid.
type DATA map[string]interface{}

// Add add the data passed overriding any existing duplicate keys.
func (d DATA) Add(data DATA) {
	for k, v := range data {
		d[k] = v
	}
}

// Save dumps the DATA as human readable JSON file at the specified path
// returning any error. File is read and write by current user only.
func (d DATA) Save(path string) error {
	return ioutil.WriteFile(path, []byte(d.String()), 0600)
}

// SaveTiny dumps the DATA as compressed JSON file at the specified path
// returning any error. File is read and write by current user only.
func (d DATA) SaveTiny(path string) error {
	return ioutil.WriteFile(path, []byte(ConvertToJSONTiny(d)), 0600)
}

// Load loads the JSON data from file at path.
func (d DATA) Load(path string) {
	byt, _ := ioutil.ReadFile(path)
	d.Parse(byt)
}

// Parse parses the JSON data from bytes passed.
func (d DATA) Parse(byt []byte) {
	json.Unmarshal(byt, d)
}

// String fulfills the Stringer interface as JSON.
func (d DATA) String() string { return ConvertToJSON(d) }

// ParseJSON reads bytes of JSON from and returns a DATA object.
func ParseJSON(byt []byte) DATA {
	data := DATA{}
	json.Unmarshal(byt, &data)
	return data
}

// LoadJSON loads a file containing JSON data specified by path into a DATA
// object. Returns empty DATA if unable to find or open the file.
func LoadJSON(path string) DATA {
	byt, _ := ioutil.ReadFile(path)
	return ParseJSON(byt)
}
