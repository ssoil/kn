# KnowledgeNet Static Site Generator

## Motivation

The main mission of the KnowledgeNet initiative is to simply and standardize mostly written content creation and maintenance in such a way that *anyone* can quickly and easily create KnowledgeNet content with very little learning curve and progressively increase the complexity of the content as needed to capture any human knowledge in written form. No such standard exists currently.

Secondarily, the KnowledgeNet seeks to fully apply all we have learned about software development, source management, packaging, signing, and distribution of source code to knowledge source text organized into *knowledge nodes* collected into *knowledge modules* that can me organized, mapped, and hyperlinked as intended by the Web's original founders.

## Design Considerations

**Single module context.** Even though consideration for multiple modules is encouraged application designs should revolve around a single module context that is active for a given user at any time. This promotes the core concept that every user has knowledge that they can create, maintain, and share including compositions of others' knowledge nodes. In the case were a user has different roles and therefore different modules to maintain they should be able to easily change the context of the current active module as if putting on another hat. This one module per user default greatly simplifies application and library implementations.

**Use Pandoc meta data terminology.** Although they might not be as popular or current terms like `keywords` and `author` (instead of `authors` are used instead of `tags` and such. This is to preserve as much Pandoc compatibility as possible.

## Meta Data

* Title >65 characters.
* Abstract (description) >150 words with no markup.

## Pandoc Markdown

The syntax of KnowledgeNet source text is 100% compatible [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown). Pandoc is already the de facto standard for [Project Gutenbreg](https://www.gutenberg.org) much other academic writing and publishing.  It is integrated fully into all R language documentation. Documents that can easily be rendered in dozens of other formats as needed. 

Unlike the original Markdown Pandoc Markdown aims to cover needs for *all* target formats rather than being HTML-centric. It also clarifies ambiguities and significantly simplifies suggested improvements to the original Markdown *such as tables*. It is the only widely used markup that includes standardized mathematical notation. It follows then that a simplified form of Pandoc Markdown, which is standard enough to be compatible with most things while progressive enough to improve on the original --- should be the standard of the KnowledgeNet.

### Pandoc Dependency

Current the `kn` tool is fundamentally dependent on the `pandoc` command, which is executed concurrently to build each node. Eventually this hard dependency may be dropped to improve build efficiency but the dependency on the Pandoc Markdown format and abstract syntax tree (AST) will always remain.

### Suggested Simplifications

Pandoc Markdown inherits many redundancies from the original Markdown that allowed for some level of artistic expression in the source text. A separator can be expressed an infinite number of ways, for example. The idea of the original Markdown was that the source text would be a readable as anything rendered from it. But this flexibility comes at great cost. It means that the source cannot be easily searched and analyzed by either content maintainers or automation. The resulting parsers are therefore extraordinarily complicated and only recently began seeing some level of standardization. 

#### Required

The following simplifications are used by the `kn` tool to *significantly* speed the parsing of headings when indexing.

* **Only include YAML at the top.** It's easier to parse and fully supported by most services and tools that are aware of "front matter."

* **Use triple-dashes for YAML front matter.** Even though YAML allows three dots to close the YAML section most "front matter" aware services and tools expect opening and closing triple-dashes.

* **Do not use any level one headings.** The `title` from the YAML is required and will be rendered as the first and only level heading. This also provides the best SEO score for Google and other search engines.

* **Use predictable fences for ignored content.** Use triple-backticks for all fenced content that you want to be ignored in indexing. In the rare cases when you are including Markdown within your fence you can also use triple-tildes or quad-tildes.

#### Suggested

Here are some suggested ways to further simplify content making it easier to read, navigate, and generally maintain:

* **Use stars.**
* **Use inline links only.**

