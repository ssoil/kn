package kn

import (
	"io/ioutil"
	"os"
	fp "path/filepath"
)

//NodePaths returns the full paths to all local nodes found in module at modpath.
//Each node directory must contain a README.md file or it will be
//ignored.
func NodePaths(modpath string) []string {
	nodes := []string{}
	inodes, err := ioutil.ReadDir(modpath)
	if err != nil {
		return nodes
	}
	for _, inode := range inodes {
		if inode.IsDir() {
			nd := fp.Join(modpath, inode.Name())
			if _, err := os.Stat(fp.Join(nd, "README.md")); err == nil {
				nodes = append(nodes, nd)
			}
		}
	}
	return nodes
}
